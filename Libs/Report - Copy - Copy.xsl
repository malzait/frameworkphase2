﻿<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!-- XSL -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">



	 <!-- HTML Head Body -->
	<xsl:output method="html"/>
		<xsl:template match="/">
			<html>
					<head>
						<link REL="StyleSheet" HREF="http://autportal/Content/Report.css" TYPE="text/css"/>						
						
						 <!-- JAVA SCRIPT Functions-->
						<script language="javascript" type="text/javascript">			  
								<![CDATA[
								function toggleMenu(id,b)
								{
									if (document.getElementById)
									{
										var e = document.getElementById(id);
										var b = document.getElementById(b);

										if (e)
										{
											if (e.style.display != "block")
											{
												e.style.display = "block";
												b.src='http://autportal/Images/check-min.JPG';
											}
											else
											{
												e.style.display = "none";
												b.src='http://autportal/Images/check-plus.JPG';
											}
										}
									}
								}						
								function expandall()
								{
									var e = document.all.tags("div");
									var b = document.all.tags("img");

									for (var i = 0; i < e.length; i++)
									{
										if (e[i].style.display == "none")
										{
											e[i].style.display = "block";
										}
									}
									for (var i = 0; i < b.length; i++)
									{
										if (b[i].id != "m1.i" && b[i].id != "m2")
										{
											if (b[i].src.substring(b[i].src.lastIndexOf("_"), b[i].src.length) == "http://autportal/Images/check-plus.JPG")
											{
												b[i].src = "http://autportal/Images/check-min.JPG";
											}
										}
									}
								}
								function collapseall()
								{
									var e = document.all.tags("div");
									var b = document.all.tags("img");

									for (var i = 0; i < e.length; i++)
									{
										if(e[i].id != "Step" && e[i].id != "Summary" && e[i].id != "Application")
										{
											if (e[i].style.display == "block")
											{
												e[i].style.display = "none";
											}
										}
									}
									for (var i = 0; i < b.length; i++)
									{
										if (b[i].id != "m1" && b[i].id != "m2")
										{
											if (b[i].src.substring(b[i].src.lastIndexOf("_"), b[i].src.length) == "http://autportal/Images/check-min.JPG")
												{
													b[i].src = "http://autportal/Images/check-min.JPG";
												}
										}
									}
								}

								function ClipBoard() 
								{
								alert("copied");
								window.clipboardData.setData("Text",document.links.txtbox1.value);
								}								


																			
									
								]]>																
						</script>
						
						 <script type="text/javascript" src="https://www.google.com/jsapi"></script>
							<script type="text/javascript">
							  google.load("visualization", "1", {packages:["corechart"]});
							  google.setOnLoadCallback(drawChart);
							  function drawChart() {
								var data = google.visualization.arrayToDataTable([
								  ['Task', 'Hours per Day'],
								  ['Did not Execute',<xsl:value-of select ="count(Report/TestSuite/TestCase[@Status='4'])"/>],
								  ['Failed',<xsl:value-of select ="count(Report/TestSuite/TestCase[@Status='2'])"/>],
								  ['Warning',<xsl:value-of select ="count(Report/TestSuite/TestCase[@Status='3'])"/>],
								  ['Passed',<xsl:value-of select ="count(Report/TestSuite/TestCase[@Status='1'])"/>],
								  
								]);
								
								var data2 = google.visualization.arrayToDataTable([
								  ['Task', 'Hours per Day'],
								  ['Failed',<xsl:value-of select ="count(Report/TestSuite/TestCase/FunctionDL/Step[@Status='2'])"/> ],
								  ['Warning',  <xsl:value-of select ="count(Report/TestSuite/TestCase/FunctionDL/Step[@Status='3'])"/> ],
								  ['Passed',<xsl:value-of select ="count(Report/TestSuite/TestCase/FunctionDL/Step[@Status='1'])"/> ],
								  
								]);
								
												
								var options = {
								  title: 'Run Chart - Test Cases',
								  is3D: true,
								  colors: ['#ffffff', '#FF0000', '#FFA500', '#008000'],
								  textStyle: {color: 'Black', fontSize: 16},
								};
								
								var options2 = {
								  title: 'Run Chart - Steps',
								  is3D: true,
								  colors: [ '#FF0000', '#FFA500', '#008000'],
								  textStyle: {color: 'Black', fontSize: 16},
								};

								var chart = new google.visualization.PieChart(document.getElementById('testcaseschart'));
								chart.draw(data,options)
								
								var chart2 = new google.visualization.PieChart(document.getElementById('stepschart'));
								chart2.draw(data2,options2)
								
							  }

							  
							</script>	
							

					</head>
				<body>          

				
						
														
				
				
				<!-- Header with weightwatchers logo-->
				
				<table width="100%">
				
					<tbody>
						<tr>							
							<td>
								<center>
								<img align="center" src="http://autportal/Images/WWAutomationReport.jpg"></img>
								</center>
									<hr width = "100%"> 	</hr>								
	<center>
<table width="100%"  align = "center">
<tr align="center">
<td>
	<center>
	<div id="psdgraphics-com-table">
		<div id="psdg-header">
			<span class="psdg-bold">Run Information</span>			
		</div>	
		
		<div id="psdg-middle">

			<div class="psdg-left"><b>Application Name:</b></div>
			<div class="psdg-right">
				<span>
					<xsl:value-of select="Report/@ApplicationName"/>
				</span>
			</div>

<div class="psdg-left"><b>Run Machine</b></div>
	<div class="psdg-right">
		<span>
			<xsl:value-of select="Report/@RunMachine"/>
		</span>
	</div>
			
			<div class="psdg-left"><b>Release:</b></div>
			<div class="psdg-right">
				<span>
					<xsl:value-of select="Report/@Release"/>
				</span>
			</div>
		
			<div class="psdg-left"><b>Track:</b></div>
			<div class="psdg-right">
				<span>
					<xsl:value-of select="Report/@Build"/>
				</span>
			</div>
			
<div class="psdg-left"><b>Round/Iteration</b></div>
	<div class="psdg-right">
		<span>
			<xsl:value-of select="Report/@Round"/>
		</span>
	</div>
			
			<div class="psdg-left"><b>Start Time :</b></div>
			<div class="psdg-right">
				<span>
					<xsl:value-of select="Report/@StartTime"/>
				</span>
			</div>			
			
			<div class="psdg-left"><b>End Time :</b></div>
			<div class="psdg-right">
			<span><xsl:value-of select="Report/@EndTime"/></span>
			</div>

			<div class="psdg-left"><b>Execute Time :</b></div>
			<div class="psdg-right">
				<span><xsl:value-of select="Report/@ExecuteHourTime"/><xsl:text> Hr(s) </xsl:text></span>
				<span><xsl:value-of select="Report/@ExecuteMinuteTime"/><xsl:text> Min(s)</xsl:text></span> 
			</div>

			<div class="psdg-left"><b>Run Against QC:</b></div>
			<div class="psdg-right">
				<span>
					<xsl:value-of select="Report/@RunQC"/>
				</span>
			</div>

			<div class="psdg-left"><b>Test Set Path:</b></div>
			<div class="psdg-right">
				<span>
					<xsl:value-of select="Report/@QCTestSetPath"/>
				</span>
			</div>

			<div class="psdg-left"><b>Test Set Name:</b></div>
			<div class="psdg-right">
				<span>
					<xsl:value-of select="Report/@QCTestSetName"/>
				</span>
			</div>



			<div id="psdg-bottom">
				<div class="psdg-bottom-cell" style="width:129px; text-align:left; padding-left: 24px;">Run Summary</div>
			</div>


			<div class="psdg-left"><b>Module/Country:</b></div>
			<div class="psdg-right">

				<xsl:for-each select="Report/TestSuite">
					<span>
						<xsl:value-of select="@Desc"/>
					</span>
				</xsl:for-each>	
			</div>

			<div class="psdg-left"><b>Passed:</b></div>
			<div class="psdg-right">

				<xsl:for-each select="Report/TestSuite">
					
						<xsl:value-of select="count(TestCase[@Status='1'])"/>
					
				</xsl:for-each>	
			</div>

			<div class="psdg-left"><b>Warning:</b></div>
			<div class="psdg-right">
				<xsl:for-each select="Report/TestSuite">
					
						<xsl:value-of select="count(TestCase[@Status='3'])"/>
					
				</xsl:for-each>		
			</div>

			<div class="psdg-left"><b>Did not execute:</b></div>
			<div class="psdg-right">
				<xsl:for-each select="Report/TestSuite">
					
						<xsl:value-of select="count(TestCase[@Status='4'])"/>
					
				</xsl:for-each>		
			</div>

			<div class="psdg-left"><b>Failed:</b></div>
			<div class="psdg-right">
				<xsl:for-each select="Report/TestSuite">
					<span>
						<xsl:value-of select="count(TestCase[@Status='2'])"/>
					</span>
				</xsl:for-each>		
			</div>


			<div class="psdg-left"><b>Total:</b></div>
			<div class="psdg-right">
				<xsl:for-each select="Report/TestSuite">
					<span>
						<xsl:value-of select="count(TestCase[@Status='1']) + count(TestCase[@Status='2']) + count(TestCase[@Status='3']) + count(TestCase[@Status='4'])" />
					</span>
				</xsl:for-each>			
			</div>
			
	</div></div>
	</center>
</td>


<td>
	<table width="100%">

		<tr>
			<td>
			<center>
				<div id="testcaseschart" style="width: 700px; height: 389px;">				
				</div>
			</center>
			</td>
		</tr>
		
		<tr>
			<td>
			<center>
				<div id="stepschart" style="width: 700px; height: 389px;">				
				</div>
			</center>
			</td>
		</tr>

	</table>
</td>

</tr>
</table>
					</center>
							</td>
							
						</tr>
					</tbody>
				</table>								

			
					
<br/><br/>
				

					
				<!-- Expand ///// Collapse For All 
				<div>
					<br/>
						<img src="https://5c0133c2-a-62cb3a1a-s-sites.googlegroups.com/site/feras13545646/check-plus.JPG" onClick="expandall()" id="m1"/>
						<xsl:text> </xsl:text>
						<a href="#" onClick="expandall()">
						<span>Expand All</span></a>
						
						
						<xsl:text> </xsl:text>
						<img src="https://5c0133c2-a-62cb3a1a-s-sites.googlegroups.com/site/feras13545646/check-min.JPG" onClick="collapseall()" id="m2"/>
						<xsl:text> </xsl:text><a href="#" onClick="collapseall()">
						<span>Collapse All</span></a>
					<br/>
				</div> -->	
					
				<!--  Apply XML Suite/   -->		
					<xsl:apply-templates select="Report/TestSuite">
					</xsl:apply-templates>					
					<br/>
					
				<!--  XSL Variables  -->	
					<xsl:variable name="lower">abcdefghijklmnopqrstuvwxyz</xsl:variable>
					<xsl:variable name="upper">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>

					<br/>
					
					




<xsl:for-each select="Report/TestSuite">														
<div Style="position:relative; display:block; text-align:center">	
<center>
<table id="Travel">
						
    <thead>    
    	<tr>
            <th scope="col" rowspan="2"></th>
            <th scope="col" colspan="6">Summary of Test Cases By Severity</th>
        </tr>
        
        <tr>
            <th scope="col">Show Stopper</th>
            <th scope="col">Very High</th>
            <th scope="col">High</th>
            <th scope="col">Medium</th>
            <th scope="col">Low</th>
            
        </tr>        
    </thead>
    
    <tfoot>
    	<tr>		
			<th scope="row">All modes</th>
			<td><xsl:value-of select="count(TestCase[@Status='1' and contains(translate(@TcSevirity,$lower,$upper), '[SHOWSTOPPER]') ])+ count(TestCase[@Status='2' and contains(translate(@TcSevirity,$lower,$upper), '[SHOWSTOPPER]') ]) + count(TestCase[@Status='3' and contains(translate(@TcSevirity,$lower,$upper), '[SHOWSTOPPER]') ]) +  count(TestCase[@Status='4' and contains(translate(@TcSevirity,$lower,$upper), '[SHOWSTOPPER]') ])"/></td>
			<td><xsl:value-of select="count(TestCase[@Status='1' and contains(translate(@TcSevirity,$lower,$upper), '[VERYHIGH]') ])+ count(TestCase[@Status='2' and contains(translate(@TcSevirity,$lower,$upper), '[VERYHIGH]') ]) + count(TestCase[@Status='3' and contains(translate(@TcSevirity,$lower,$upper), '[VERYHIGH]') ]) +  count(TestCase[@Status='4' and contains(translate(@TcSevirity,$lower,$upper), '[VERYHIGH]') ])"/></td>
			<td><xsl:value-of select="count(TestCase[@Status='1' and contains(translate(@TcSevirity,$lower,$upper), '[HIGH]') ])+ count(TestCase[@Status='2' and contains(translate(@TcSevirity,$lower,$upper), '[HIGH]') ]) + count(TestCase[@Status='3' and contains(translate(@TcSevirity,$lower,$upper), '[HIGH]') ]) +  count(TestCase[@Status='4' and contains(translate(@TcSevirity,$lower,$upper), '[HIGH]') ])"/></td>
			<td><xsl:value-of select="count(TestCase[@Status='1' and contains(translate(@TcSevirity,$lower,$upper), '[MEDIUM]') ])+ count(TestCase[@Status='2' and contains(translate(@TcSevirity,$lower,$upper), '[MEDIUM]') ]) + count(TestCase[@Status='3' and contains(translate(@TcSevirity,$lower,$upper), '[MEDIUM]') ]) +  count(TestCase[@Status='4' and contains(translate(@TcSevirity,$lower,$upper), '[MEDIUM]') ])"/></td>
			<td><xsl:value-of select="count(TestCase[@Status='1' and contains(translate(@TcSevirity,$lower,$upper), '[LOW]') ])+ count(TestCase[@Status='2' and contains(translate(@TcSevirity,$lower,$upper), '[LOW]') ]) + count(TestCase[@Status='3' and contains(translate(@TcSevirity,$lower,$upper), '[LOW]') ]) +  count(TestCase[@Status='4' and contains(translate(@TcSevirity,$lower,$upper), '[LOW]') ])"/></td>
		</tr>
    </tfoot>
    
    <tbody>
    	<tr class="">
    		<th scope="row">Total Passed</th>
            <td><xsl:value-of select="count(TestCase[@Status='1' and contains(translate(@TcSevirity,$lower,$upper), '[SHOWSTOPPER]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='1' and contains(translate(@TcSevirity,$lower,$upper), '[VERYHIGH]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='1' and contains(translate(@TcSevirity,$lower,$upper), '[HIGH]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='1' and contains(translate(@TcSevirity,$lower,$upper), '[MEDIUM]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='1' and contains(translate(@TcSevirity,$lower,$upper), '[LOW]') ])"/></td>
            
        </tr>


     
        <tr class="">
        	<th scope="row">Total Failed</th>
            <td><xsl:value-of select="count(TestCase[@Status='2' and contains(translate(@TcSevirity,$lower,$upper), '[SHOWSTOPPER]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='2' and contains(translate(@TcSevirity,$lower,$upper), '[VERYHIGH]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='2' and contains(translate(@TcSevirity,$lower,$upper), '[HIGH]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='2' and contains(translate(@TcSevirity,$lower,$upper), '[MEDIUM]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='2' and contains(translate(@TcSevirity,$lower,$upper), '[LOW]') ])"/></td>
            
        </tr>



    
        <tr class="">
        	<th scope="row">Total Warning</th>
            <td><xsl:value-of select="count(TestCase[@Status='3' and contains(translate(@TcSevirity,$lower,$upper), '[SHOWSTOPPER]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='3' and contains(translate(@TcSevirity,$lower,$upper), '[VERYHIGH]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='3' and contains(translate(@TcSevirity,$lower,$upper), '[HIGH]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='3' and contains(translate(@TcSevirity,$lower,$upper), '[MEDIUM]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='3' and contains(translate(@TcSevirity,$lower,$upper), '[LOW]') ])"/></td>
            
        </tr>
        
        <tr class="">
        	<th scope="row">Total Didn't Execute</th>
            <td><xsl:value-of select="count(TestCase[@Status='4' and contains(translate(@TcSevirity,$lower,$upper), '[SHOWSTOPPER]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='4' and contains(translate(@TcSevirity,$lower,$upper), '[VERYHIGH]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='4' and contains(translate(@TcSevirity,$lower,$upper), '[HIGH]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='4' and contains(translate(@TcSevirity,$lower,$upper), '[MEDIUM]') ])"/></td>
            <td><xsl:value-of select="count(TestCase[@Status='4' and contains(translate(@TcSevirity,$lower,$upper), '[LOW]') ])"/></td>
            
        </tr>
        
        
        
        
        
        
        
    </tbody>

</table>

</center>					
					
</div>
</xsl:for-each>
					

					
				





		



					
					
			   
				<!-- Footer  -->	
				 <center>
					<div id="footer_wrapper">
						<div id="footer"> 
							 2013 Aspire - Automation | 
							<a href="" target="_blank">Design by Firas Khorma</a> |
							<a href="">Automation Engineer</a> 
						 </div>
					</div>
				 </center>
					
					

				 </body>				 				
			</html>		 	
		</xsl:template>		
	 <!-- /HTML Head Body -->
		
		
	
		
		
					

		 <!-- Suite Node Application Name With Country -->
		<xsl:template match="Report/TestSuite">
			<div Style="position:relative; display:block" class="CSS_Table_Example">
				<table id="TestSuite" cellSpacing="1" cellPadding="1"  onClick="toggleMenu('div{position()}.0', 'm{position()}.0')">			
					<tr>
						<td>
							<img id="m{position()}.0" src="http://autportal/Images/check-plus.JPG" border="0"/>
							<xsl:text> </xsl:text><xsl:value-of select="@Desc"/>
						</td>
							<td class="SuiteStatusPassed">Passed -
							 <xsl:value-of select="count(TestCase[@Status='1'])"/> 
							</td>
							<td class="SuiteStatusFailed">Failed -
							 <xsl:value-of select="count(TestCase[@Status='2']) " /> 
							</td>		
							<td class="SuiteStatusWarning">Warning -
							 <xsl:value-of select="count(TestCase[@Status='3']) " /> 
							</td>	
							<td class="SuiteStatusDone">Did not execute -
							 <xsl:value-of select="count(TestCase[@Status='4']) " /> 
							</td>	
					</tr>
				</table>
			</div>
			
			<br/>
			<div id="div{position()}.0" Style="display:none">
				<xsl:apply-templates select="TestCase">
				<xsl:with-param name="TestCasePosition" select="position()"/>
				</xsl:apply-templates>
			</div>
		</xsl:template>
		<!-- Suite Node Application Name With Country -->
		
		
	
		
		
			
		<!-- Test Case Node With Sevirity -->
		<xsl:template match="TestCase">		
			<xsl:param name="TestCasePosition"/>	
				<div id="TestCase"  Style="left:10px; position: relative; display:block">
					<table cellSpacing="1" cellPadding="1" align="center" Class="TestCase" onClick="toggleMenu('div{concat($TestCasePosition,'.',position())}', 'm{concat($TestCasePosition,'.',position())}')">
						<tr>
							<td class="message">
			
								<img id="m{concat($TestCasePosition,'.',position())}" src="http://autportal/Images/check-plus.JPG" border="0"/>																
								<xsl:text> </xsl:text><xsl:value-of select="@TcName"/>
								<xsl:text> </xsl:text><xsl:value-of select="@TcSevirity"/>
								<span style="display: none">
									<xsl:text> </xsl:text><xsl:value-of select="@Status"/>
								</span>
							</td>
							
							
							 <xsl:element name="td">

								  <xsl:attribute name="class">
									<xsl:choose>
									  <xsl:when test="@Status='2'">
										StatusFailed	
									  </xsl:when>
									  <xsl:when test="@Status='3'">
										StatusWarning	
									  </xsl:when>
									  <xsl:when test="@Status='4'">
										StatusDone
									  </xsl:when>
									  <xsl:otherwise>
										StatusPassed												
									  </xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
									<xsl:choose>
									  <xsl:when test="@Status='2'">
										Failed	<xsl:text> </xsl:text><xsl:value-of select="@TcDefectID"/>
									  </xsl:when>
									  <xsl:when test="@Status='3'">
										Warning	
									  </xsl:when>
									  <xsl:when test="@Status='4'">
										Did not execute	
									  </xsl:when>
									  <xsl:otherwise>
										Passed												
									  </xsl:otherwise>
									</xsl:choose>
									
									
								<xsl:text> </xsl:text><xsl:value-of select="@Desc"/>
							  </xsl:element>

		
						</tr>	                    		
					</table>																
				</div>
				
				

				<div id="div{concat($TestCasePosition,'.',position())}" Style="display:none">																									   
					<xsl:apply-templates select="FunctionDL">					
					</xsl:apply-templates>   
				</div>
		</xsl:template>
		<!-- Test Case Node With Sevirity -->
		
		
	
		
		
			
		<!-- Function Node  -->
		<xsl:template match="FunctionDL">
				<xsl:param name="FunctionPosition"/>		
					<div id="FunctionDL" Style="left:10px; position: relative; display:block">

						<table cellSpacing="1" cellPadding="1" align="center" Class="FunctionDL" onClick="toggleMenu('div{concat($FunctionPosition@DateTime,'.',position())}', 'm{concat($FunctionPosition@DateTime,'.',position())}')">
							<tr>
								<td class="message">
									<img id="m{concat($FunctionPosition@DateTime,'.',position())}" src="http://autportal/Images/check-plus.JPG" border="0"/>
									<xsl:text> </xsl:text><xsl:value-of select="@ID"/>
									<xsl:text> </xsl:text><xsl:value-of select="@Desc"/>
								</td>
								 <xsl:element name="td">
									  <xsl:attribute name="class">
										<xsl:if test="count(Step[@Status='2']) = 0 and count(Step[@Status='3']) = 0 and count(Step[@Status='4']) = 0">
										StatusPassed
										</xsl:if>
										<xsl:if test="count(Step[@Status='2']) > 0">
										StatusFailed
										</xsl:if>
										<xsl:if test="count(Step[@Status='3']) > 0 and count(Step[@Status='1']) = 0 and count(Step[@Status='2']) = 0">
										StatusWarning
										</xsl:if>
										<xsl:if test="count(Step[@Status='4']) > 0 and count(Step[@Status='3']) = 0 and count(Step[@Status='1']) = 0 and count(Step[@Status='2']) = 0">
										StatusDone
										</xsl:if>
									</xsl:attribute>
									<xsl:if test="count(Step[@Status='2']) = 0 and count(Step[@Status='3']) = 0 and count(Step[@Status='4']) = 0">
										<xsl:text>Passed </xsl:text>
									</xsl:if>
									<xsl:if test="count(Step[@Status='2']) > 0">
										<xsl:text>Failed </xsl:text>
									</xsl:if>
									<xsl:if test="count(Step[@Status='3']) > 0 and count(Step[@Status='1']) = 0 and count(Step[@Status='2']) = 0">
										<xsl:text>Warning </xsl:text>
									</xsl:if>
									<xsl:if test="count(Step[@Status='4']) > 0 and count(Step[@Status='3']) = 0 and count(Step[@Status='1']) = 0 and count(Step[@Status='2']) = 0">
										Done
										</xsl:if>
								  </xsl:element>									  																 
						
							</tr>											
						</table>												
					</div>

					<div id="div{concat($FunctionPosition@DateTime,'.',position())}" Style="display:none">	
					<xsl:for-each select="TestCase"/>
					   <table cellSpacing="1" Class="Step" cellPadding="1" align="center">
						<tr>
						 <td width="27%">
						  <center>Step Detail(s)</center>
						 </td>
						 <td width="26%">
						  <center>Actual Result</center>
						 </td>
						 <td width="26%%">
						  <center>Expected Result</center>
						 </td>
						 <td>
						  <center>Screenshot</center>
						 </td>
						</tr>
					   </table>
						<xsl:apply-templates select="Step">
						</xsl:apply-templates>				
					</div>
		</xsl:template>
		<!-- Function Node  -->
		
		
	
		
		
			 <xsl:template match="Step">
			  <div style="left:10px; position: relative; display:block">			   
				<table Class="Step" align="center">
					<tr>				
						<xsl:if test="position()mod 2">
							<xsl:attribute name="class">
							M1
							</xsl:attribute>
							</xsl:if>

							<xsl:if test="not(position()mod 2)">
							<xsl:attribute name="class">
							M2
							</xsl:attribute>
						</xsl:if>

											 
						<xsl:element name="td">
							<xsl:attribute name="class">
							
								<xsl:if test="@Status='1'">
									StatusPassed									
								</xsl:if>
							
								<xsl:if test="@Status='2'">
									StatusFailed
								</xsl:if>
								
								<xsl:if test="@Status='3'">
									StatusWarning
								</xsl:if>
								
								<xsl:if test="@Status='4'">
									StatusDone
								</xsl:if>
								
							</xsl:attribute>
							
							<xsl:text>-</xsl:text>
							
							<xsl:value-of select="."/>
						</xsl:element>    
						
						
						<xsl:if test="@Detail">
							<xsl:element name="td">
								<xsl:attribute name="class">
									<xsl:if test="@Status='1'">
										StatusPassed
									</xsl:if>
									<xsl:if test="@Status='2'">
										StatusFailed
									</xsl:if>
									<xsl:if test="@Status='3'">
										StatusWarning
									</xsl:if>	
									<xsl:if test="@Status='4'">
										StatusDone
									</xsl:if>	
									
								</xsl:attribute>
						   <center> <xsl:text> </xsl:text><xsl:value-of select="@Detail"/></center>
							</xsl:element>
						</xsl:if>
						
						
						
					<xsl:element name="td">		



							<xsl:if test="@ActualResult">
							   
									<xsl:attribute name="class">
										<xsl:if test="@Status='1'">
											StatusPassed
										</xsl:if>
										<xsl:if test="@Status='2'">
											StatusFailed
										</xsl:if>
										<xsl:if test="@Status='3'">
											StatusWarning
										</xsl:if>
										<xsl:if test="@Status='4'">
											StatusDone
										</xsl:if>
									</xsl:attribute>
									
										<xsl:text> </xsl:text><xsl:value-of select="@ActualResult"/> 	
										
				

				
																	
							</xsl:if>
	

											
					</xsl:element>   
						
						
						
						<xsl:element name="td">
							<xsl:if test="@ExpectedResult">                        
									<xsl:attribute name="class">
										<xsl:if test="@Status='1'">
											StatusPassed
										</xsl:if>
										<xsl:if test="@Status='2'">
											StatusFailed
										</xsl:if>
										<xsl:if test="@Status='3'">
											StatusWarning
										</xsl:if>
										<xsl:if test="@Status='4'">
											StatusDone
										</xsl:if>
									</xsl:attribute>
								<center><xsl:text> </xsl:text><xsl:value-of select="@ExpectedResult"/></center>                        
							</xsl:if>
						</xsl:element>    
						

							
						<xsl:element name="td">
							<xsl:choose>
								<xsl:when test="@ScreenShotPath">																
										<xsl:attribute name="class">
											LinkToFile
										</xsl:attribute>
									<center><a href="{@ScreenShotPath}" target="_new">Screenshot</a></center>									
								</xsl:when>
								<xsl:otherwise>		
									<xsl:attribute name="class">
										LinkToFile
										</xsl:attribute>
									<center>-</center>													
								</xsl:otherwise>
							</xsl:choose>	
						</xsl:element>   

					</tr>
					
				</table>
			</div>
		</xsl:template>
		<!-- Steps List Under FunctionDL -->
		
		
	
		
		
			
		<!-- File Name -->
		<xsl:template name="GetFileName">
		<xsl:param name="filepath"/>
			<xsl:variable name="filename" select="substring-after($filepath,'\')"/>
			<xsl:choose>
			<xsl:when test="contains($filename,'\')">
			  <xsl:call-template name="GetFileName">
				  <xsl:with-param name="filepath" select="$filename"/>
			  </xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
				<a href="{@Filepath}" target="_new"><xsl:value-of select="$filename"/></a>
			</xsl:otherwise>
			</xsl:choose>
		</xsl:template>
		<!-- File Name -->
		
		
		
	
	
</xsl:stylesheet>
<!-- XSL -->